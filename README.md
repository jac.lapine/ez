# EZ

The EZ configuration and serialized object notation. Example:

	(max-file-size: 2,000MB
	 timeout:       30-secs
	 tmp-files:     /var/tmp)
	
EZ is:

 - *Simple.* The syntax for the EZ notation fits on a single page. 
 - *Readable.* Even without a reference, the meaning of an EZ configuration file is obvious:
 - *Expressive.* EZ understands the unit you use everyday, uses common-sense by default, and is tolerant of variations while never being ambiguous.
 - *Extensible.* Configure EZ behavior via a profile, add your own units, turn on/off features to customization to your environment.
 - *Safe.* EZ is not executable and does not need any form of code guards to be safe.
 - *Zero Dependencies.* EZ is packaged as a single deployment unit.

The quick reference can be found here:

	/doc/Ez Quick Reference.png

Bindings for Java and other languages in the /bindings directory.



